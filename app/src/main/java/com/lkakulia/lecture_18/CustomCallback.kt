package com.lkakulia.lecture_18

interface CustomCallback {
    fun onFailure(message: String)
    fun onResponse(response: String)
}