package com.lkakulia.lecture_18

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private var items: MutableList<UserModel.DataModel>): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recyclerview_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private lateinit var model: UserModel.DataModel

        fun onBind() {
            model = items[adapterPosition]
            Glide.with(itemView.context)
                .load(model.avatar)
                .placeholder(R.drawable.ic_launcher_background)
                .into(itemView.avatarImageView)
            itemView.idTextView.text = model.id.toString()
            itemView.emailTextView.text = model.email
            itemView.firstNameTextView.text = model.firstName
            itemView.lastNameTextView.text = model.lastName

        }
    }
}