package com.lkakulia.lecture_18

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var items = mutableListOf<UserModel.DataModel>()
    private lateinit var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
    private lateinit var users: UserModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun getUsers() {
        HttpRequest.getRequest(HttpRequest.USERS, object : CustomCallback {
            override fun onFailure(message: String) {
                d("onFailure", " ${message}")
            }

            override fun onResponse(response: String) {
                parseJson(response)
            }

        })
    }

    private fun init() {
        getUsers()
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener({
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                getUsers()
                adapter.notifyDataSetChanged()
            }, 2000)
        })
    }

    private fun parseJson(response: String) {
        users = UserModel()
        val joson = JSONObject(response)
        val ad = joson.getJSONObject("ad")

        if (ad.has("company"))
            companyTextView.text = ad.getString("company")
        if(ad.has("url"))
            urlTextView.text = ad.getString("url")
        if(ad.has("text"))
            textTextView.text = ad.getString("text")

        if(joson.has("page"))
            users.page = joson.getInt("page")
        if(joson.has("per_page"))
            users.perPage = joson.getInt("per_page")
        if(joson.has("total"))
            users.total = joson.getInt("total")
        if(joson.has("total_pages"))
            users.totalPages = joson.getInt("total_pages")

        val jsonObjectArray = joson.getJSONArray("data")
        for (i in 0 until jsonObjectArray.length()) {
            val jsonObject = jsonObjectArray[i] as JSONObject
            val data = UserModel.DataModel()

            if(jsonObject.has("id"))
                data.id = jsonObject.getInt("id")
            if(jsonObject.has("email"))
                data.email = jsonObject.getString("email")
            if(jsonObject.has("first_name"))
                data.firstName = jsonObject.getString("first_name")
            if(jsonObject.has("last_name"))
                data.lastName = jsonObject.getString("last_name")
            if(jsonObject.has("avatar"))
                data.avatar = jsonObject.getString("avatar")

            users.data.add(data)
            items.add(data)
        }

    }

    private fun refresh() {
        items.clear()
        adapter.notifyDataSetChanged()
    }

}
