package com.lkakulia.lecture_18

class UserModel {
    var page = 0
    var perPage = 0
    var total = 0
    var totalPages = 0
    var data = mutableListOf<DataModel>()

    class DataModel {
        var id = 0
        var email = ""
        var firstName = ""
        var lastName = ""
        var avatar = ""
    }
}