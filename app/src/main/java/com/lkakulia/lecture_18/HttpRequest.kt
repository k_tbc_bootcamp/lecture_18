package com.lkakulia.lecture_18

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

object HttpRequest {

    const val USERS = "users"
    const val URL = "https://reqres.in/api/"

    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(URL)
        .build()

    private var service = retrofit.create(ApiService::class.java)

    fun getRequest(path: String, callback: CustomCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallBack(callback))
    }

    private fun onCallBack(callback: CustomCallback) = object: Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("onFailure", " ${t.message}")
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("response", " ${response.body()}")
            callback.onResponse(response.body().toString())
        }

    }

    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): Call<String>

        @POST("{path}")
        fun postRequest(@Path("path") path: String): Call<String>
    }
}